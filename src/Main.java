public class Main {

    public static void main(String[] args) {
        String textOriginal = "Дорого небо, да надобен огород!";
        int compareInt;

        textOriginal = textOriginal.replace("!", "");
        textOriginal = textOriginal.replace("?", "");
        textOriginal = textOriginal.replace(".", "");
        textOriginal = textOriginal.replace(",", "");
        textOriginal = textOriginal.replace(" ", "");
        textOriginal = textOriginal.toLowerCase();

        StringBuilder stringBuilder = new StringBuilder(textOriginal);
        StringBuilder textAfter = new StringBuilder(textOriginal);

        compareInt = textAfter.compareTo(stringBuilder.reverse());

        if (compareInt == 0) {
            System.out.println("Пилиндром, отвечаю");
        } else {
            System.out.println("НЕ пилиндром");

        }


    }
}
